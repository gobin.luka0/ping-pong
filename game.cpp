// Macros
#define is_down(b) input->buttons[b].is_down
#define pressed(b) (input->buttons[b].is_down && input->buttons[b].changed)
#define released(b) (!input->buttons[b].is_down && input->buttons[b].changed)

// Player position and deriv position for acceleration
float player_1_p, player_1_dp, player_2_p, player_2_dp;
float arena_half_size_x = 85, arena_half_size_y = 45;
float player_half_size_x = 2.5, player_half_size_y = 12;
float ball_p_x, ball_p_y, ball_dp_x = 100, ball_dp_y, ball_half_size = 1;
int score_p_1 = 0, score_p_2 = 0;

internal void simulate_player(float *p, float *dp, float ddp, float dt) {
    ddp -= *dp * 10.f;

    *p = *p + *dp * dt + ddp * dt * dt * .5f;
    *dp = *dp + ddp * dt;

    // Arena collison check for p1
    if (*p + player_half_size_y > arena_half_size_y) {
        *p = arena_half_size_y - player_half_size_y;
        *dp = 0;
    } else if (*p - player_half_size_y < -arena_half_size_y) {
        *p = -arena_half_size_y + player_half_size_y;
        *dp = 0;
    }
}

internal bool aabb_vs_bbaa(float p1x, float p1y, float hs1x, float hs1y,
    float p2x, float p2y, float hs2x, float hs2y) {
    return (p1x - hs1x > p2x - hs2x &&
            p1x - hs1x < p2x + hs2x &&
            p1y + hs1y > p2y - hs2y &&
            p1y + hs1y < p2y + hs2y);
}

enum Gamemode {
    GM_MENU,
    GM_GAMEPLAY
};

Gamemode current_gamemode;
int hot_button;
bool enemy_is_ai;

// TASK - Make player accelerate when holding a button for a longer period of time
// TASK - Implement score system
internal void simulate_game(Input* input, float dt) {
    clear_screen(0xff5500);
    draw_rectangle(0, 0, arena_half_size_x, arena_half_size_y, 0xffaa33);

    if (current_gamemode == GM_GAMEPLAY) {

        if (pressed(BUTTON_ESCAPE)) {
            current_gamemode = GM_MENU;
            score_p_1 = player_1_p = player_1_dp = 0;
            score_p_2 = player_2_p = player_2_dp = 0;
            ball_p_x = ball_p_y = ball_dp_y = 0;

            ball_dp_x = -100;
        }

        float player_1_ddp = 0.f;
        // -> because it is a pointer
        // PROBLEM: Make movements speed as units/second, can't be dependent on the frame rate, fixed with dt
        if(is_down(BUTTON_W)) player_1_ddp += 700;
        if(is_down(BUTTON_S)) player_1_ddp -= 700;

        float player_2_ddp = 0.f;

        // Case 2P / AI
        if (enemy_is_ai){
            if(ball_p_y > player_2_p + 2.f) player_2_ddp += 500;
            else if (ball_p_y < player_2_p - 2.f) player_2_ddp -= 500;
        } else {
            if(is_down(BUTTON_UP)) player_2_ddp += 700;
            if(is_down(BUTTON_DOWN)) player_2_ddp -= 700;
        }

        simulate_player(&player_1_p, &player_1_dp, player_1_ddp, dt);
        simulate_player(&player_2_p, &player_2_dp, player_2_ddp, dt);

        // Ball logic
        {
            ball_p_x += ball_dp_x * dt;
            ball_p_y += ball_dp_y * dt;

            // Detecting ball collision - player
            // Could move this aabb check out in the function
            if (aabb_vs_bbaa(ball_p_x, ball_p_y, ball_half_size, ball_half_size, 80, player_2_p, player_half_size_x, player_half_size_y)) {
                ball_p_x = 80 - player_half_size_x - ball_half_size;
                ball_dp_x *= -1;
                // Making ball take vertical speed of the player colliding with it
                ball_dp_y = (ball_p_y - player_2_p) * 2 + player_2_dp * .75f;
            } else if (aabb_vs_bbaa(ball_p_x, ball_p_y, ball_half_size, ball_half_size, -80, player_1_p, player_half_size_x, player_half_size_y)) {
                ball_p_x = -80 + player_half_size_x + ball_half_size;
                ball_dp_x *= -1;
                ball_dp_y = (ball_p_y - player_1_p) * 2 + player_1_dp * .75f;
            }

            // Detecting ball-wall collision
            if (ball_p_y + ball_half_size > arena_half_size_y) {
                ball_p_y = arena_half_size_y - ball_half_size;
                ball_dp_y *= -1;
            } else if (ball_p_y - ball_half_size < -arena_half_size_y) {
                ball_p_y = -arena_half_size_y + ball_half_size;
                ball_dp_y *= -1;
            }

            // Adding ball respawn when out of the field
            if (ball_p_x + ball_half_size > arena_half_size_x) {
                ball_dp_x *= -1;
                ball_dp_y = 0;
                ball_p_x = 0;
                ball_p_y = 0;
                score_p_1 ++;
            } else if (ball_p_x - ball_half_size < -arena_half_size_x) {
                ball_dp_x *= -1;
                ball_dp_y = 0;
                ball_p_x = 0;
                ball_p_y = 0;
                score_p_2 ++;
            }
        }

        draw_number(score_p_1, -10, 40, 1.f, 0xbbffbb);
        draw_number(score_p_2, 10, 40, 1.f, 0xbbffbb);

        // Drawing players and the ball in correct positions
        draw_rectangle(-80, player_1_p, player_half_size_x, player_half_size_y, 0xff0000);
        draw_rectangle(80, player_2_p, player_half_size_x, player_half_size_y, 0xff0000);
        draw_rectangle(ball_p_x, ball_p_y, ball_half_size, ball_half_size, 0xffffff);
    } else {
        if (pressed(BUTTON_LEFT) || pressed(BUTTON_RIGHT)) {
            hot_button = !hot_button;
        }

        if (pressed(BUTTON_ENTER)) {
            current_gamemode = GM_GAMEPLAY;
            enemy_is_ai = hot_button ? 0 : 1;
        }

        if (hot_button == 0) {
            draw_text("SINGLEPLAYER", -60, 0, .8f, 0xff0000);
            draw_text("MULTIPLAYER", 10, 0, .8f, 0xcccccc);
        } else {
            draw_text("SINGLEPLAYER", -60, 0, .8f, 0xcccccc);
            draw_text("MULTIPLAYER", 10, 0, .8f, 0xff0000);
        }

        draw_text("PING PONG GAME", -40, 30, 1, 0xffffff);
    }
}