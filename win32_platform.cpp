// Note for myself since I'm writing this code from Ubuntu...
// Compile with: x86_64-w64-mingw32-g++ win32_platform.cpp -o test.exe -lgdi32
#include "utils.cpp"
#include <windows.h>

bool running { true };

struct Render_State {
    int height, width;
    void* memory;

    BITMAPINFO bitmapinfo;
};

global_variable Render_State render_state;

// Including our file with rendering functions
#include "platform_common.cpp"
#include "renderer.cpp"
#include "game.cpp"

// Contains info that is sent to StretchDIBits for rendering
LRESULT CALLBACK window_callback(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    LRESULT result = 0;

    switch(uMsg) {
        // If window receives a termination message
        case WM_CLOSE:
        case WM_DESTROY: {
            running = false;
        } break;

        // If window size change occurs
        case WM_SIZE: {
            // Rectange class rect
            RECT rect;

            // Pass rect class into GetClientRect function to receive window properties
            GetClientRect(hwnd, &rect);

            // Calculate width and height
            render_state.width = rect.right - rect.left;
            render_state.height = rect.bottom - rect.top;

            // Total buffer size depending on a resolution (32 bits per pixel)
            int buffer_size = render_state.width * render_state.height * sizeof(u32);

            // Free render_state.memory if it exists so that it can be allocated again
            if (render_state.memory)  VirtualFree(render_state.memory, 0, MEM_RELEASE);

            // Allocating the memory (VirtualAlloc - similar to malloc, but windows specific)
            render_state.memory = VirtualAlloc(0, buffer_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

            // Filling up the render_state.bitmapinfo header
            render_state.bitmapinfo.bmiHeader.biSize = sizeof(render_state.bitmapinfo.bmiHeader);
            render_state.bitmapinfo.bmiHeader.biWidth = render_state.width;
            render_state.bitmapinfo.bmiHeader.biHeight = render_state.height;
            render_state.bitmapinfo.bmiHeader.biPlanes = 1;
            render_state.bitmapinfo.bmiHeader.biBitCount = 32;
            render_state.bitmapinfo.bmiHeader.biCompression = BI_RGB;
        } break;
        default: {
            result = DefWindowProc(hwnd, uMsg, wParam, lParam);
        }
    }

    return result;
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {
    // Create Window Class
    WNDCLASS window_class = {};
    window_class.style = CS_HREDRAW | CS_VREDRAW;
    window_class.lpfnWndProc = window_callback; // Corrected the member name
    window_class.hInstance = hInstance;
    window_class.lpszClassName = "Game Window Class"; // Corrected the member name
    
    // Register Class
    RegisterClass(&window_class);

    // Create Window
    HWND win = CreateWindow(window_class.lpszClassName, "Ping Pong Game", WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720, 0, 0, hInstance, 0);
    HDC hdc = GetDC(win);

    Input input = {};

    // Adding SPF tracking
    float delta_time = 0.016666f;
    // Measuring CPU time at the start of the frame
    LARGE_INTEGER frame_begin_time;
    QueryPerformanceCounter(&frame_begin_time);

    // Function that returns NO of CPU cycles in 1 second
    float performace_frequency;
    {
        LARGE_INTEGER perf;
        QueryPerformanceFrequency(&perf);
        performace_frequency = (float)perf.QuadPart;
    }

    while (running) {
        // Input
        MSG message;

        // Setting changed to false for all of the buttons at the start of every frame
        for (int i = 0; i < BUTTON_COUNT; i++) {
            input.buttons[i].changed = false;
        }

        while (PeekMessage(&message, win, 0, 0, PM_REMOVE)) {
            // Processing user input
            switch(message.message) {
                case WM_KEYUP:
                case WM_KEYDOWN: {
                    // Code of a button
                    u32 vk_code = (u32)message.wParam;

                    // Default check for if key is pressed down
                    bool is_down = ((message.lParam & (1 << 31)) == 0);

// Adding macro for button processing
#define process_button(b, vk)\
case vk: {\
    input.buttons[b].is_down = is_down;\
    input.buttons[b].changed = true;\
} break;

                    switch(vk_code) {
                        process_button(BUTTON_UP, VK_UP);
                        process_button(BUTTON_DOWN, VK_DOWN);
                        process_button(BUTTON_W, 'W');
                        process_button(BUTTON_S, 'S');
                        process_button(BUTTON_LEFT, VK_LEFT);
                        process_button(BUTTON_RIGHT, VK_RIGHT);
                        process_button(BUTTON_ENTER, VK_RETURN);
                        process_button(BUTTON_ESCAPE, VK_ESCAPE);
                    }
                } break;
                default: {
                    TranslateMessage(&message);
                    DispatchMessage(&message);
                }
            }
        }

        // Simulate

        // Moving logic inside game simulation file
        simulate_game(&input, delta_time);

        // Render
        // Asking Windows to use allocated memory for the window
        StretchDIBits(hdc, 0, 0, render_state.width, render_state.height, 0, 0, render_state.width, render_state.height, render_state.memory, &render_state.bitmapinfo, DIB_RGB_COLORS, SRCCOPY);

        // Measuring CPU time at the end of the frame
        LARGE_INTEGER frame_end_time;
        QueryPerformanceCounter(&frame_end_time);
        // Getting the difference between end and start time of the frame
        // Divide with performace_frequency to get the time in seconds
        delta_time = (float)(frame_end_time.QuadPart - frame_begin_time.QuadPart) / performace_frequency;
        frame_begin_time = frame_end_time;
    }

    return 0;
}
