// File with rendering functions

internal void clear_screen(u32 color) {
    // Setting up pointer on render_state.memory on first pixel
    u32* pixel = (u32*)render_state.memory;

    // Looping through all of the pixels
    for (int y = 0; y < render_state.height; y++) {
        for (int x = 0; x < render_state.width; x++) {
            // Moving pointer to the next pixel after setting up color to specified color
            *pixel++ = color;
        }
    }
}

internal void draw_rectangle_in_pixels(int x0, int y0, int x1, int y1, u32 color) {
    // Fix for the crash if screen becomes smaller than the rectangle
    x0 = clamp(0, x0, render_state.width);
    x1 = clamp(0, x1, render_state.width);
    y0 = clamp(0, y0, render_state.height);
    y1 = clamp(0, y1, render_state.height);

    for (int y = y0; y < y1; y++) {
        u32* pixel = (u32*)render_state.memory + x0 + y * render_state.width;
        for (int x = x0; x < x1; x++) {
            *pixel++ = color;
        }
    }
}

global_variable float render_scale = 0.01f;

// Function that will scale rectangle by the smaller dimension
internal int determine_scale() {
    if (render_state.width >= render_state.height) return render_state.height;

    return render_state.width;
}

internal void draw_rectangle(float x, float y, float half_size_x, float half_size_y, u32 color) {
    int scale { determine_scale() };
    // Making width and height relative to the screen size
    x *= scale * render_scale;
    y *= scale * render_scale;
    half_size_x *= scale * render_scale;
    half_size_y *= scale * render_scale;

    // Centering the rectangle
    x += render_state.width / 2.f;
    y += render_state.height / 2.f;

    int x0 = x - half_size_x;
    int x1 = x + half_size_x;
    int y0 = y - half_size_y;
    int y1 = y + half_size_y;

    draw_rectangle_in_pixels(x0, y0, x1, y1, color);
}

const char* letters[][7] = {
    " 00",
    "0  0",
    "0  0",
    "0000",
    "0  0",
    "0  0",
    "0  0",

	"000",
	"0  0",
	"0  0",
	"000",
	"0  0",
	"0  0",
	"000",

	" 000",
	"0",
	"0",
	"0",
	"0",
	"0",
	" 000",

	"000",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"000",

	"0000",
	"0",
	"0",
	"000",
	"0",
	"0",
	"0000",

	"0000",
	"0",
	"0",
	"000",
	"0",
	"0",
	"0",

	" 000",
	"0",
	"0",
	"0 00",
	"0  0",
	"0  0",
	" 000",

	"0  0",
	"0  0",
	"0  0",
	"0000",
	"0  0",
	"0  0",
	"0  0",

	"000",
	" 0",
	" 0",
	" 0",
	" 0",
	" 0",
	"000",

	" 000",
	"   0",
	"   0",
	"   0",
	"0  0",
	"0  0",
	" 000",

	"0  0",
	"0  0",
	"0 0",
	"00",
	"0 0",
	"0  0",
	"0  0",

	"0",
	"0",
	"0",
	"0",
	"0",
	"0",
	"0000",

	"00 00",
	"0 0 0",
	"0 0 0",
	"0   0",
	"0   0",
	"0   0",
	"0   0",

	"00  0",
	"0 0 0",
	"0 0 0",
	"0 0 0",
	"0 0 0",
	"0 0 0",
	"0  00",

	"0000",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0000",

	" 000",
	"0  0",
	"0  0",
	"000",
	"0",
	"0",
	"0",

	" 000 ",
	"0   0",
	"0   0",
	"0   0",
	"0 0 0",
	"0  0 ",
	" 00 0",

	"000",
	"0  0",
	"0  0",
	"000",
	"0  0",
	"0  0",
	"0  0",

	" 000",
	"0",
	"0 ",
	" 00",
	"   0",
	"   0",
	"000 ",

	"000",
	" 0",
	" 0",
	" 0",
	" 0",
	" 0",
	" 0",

	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	" 00",

	"0   0",
	"0   0",
	"0   0",
	"0   0",
	"0   0",
	" 0 0",
	"  0",

	"0   0 ",
	"0   0",
	"0   0",
	"0 0 0",
	"0 0 0",
	"0 0 0",
	" 0 0 ",

	"0   0",
	"0   0",
	" 0 0",
	"  0",
	" 0 0",
	"0   0",
	"0   0",

	"0   0",
	"0   0",
	" 0 0",
	"  0",
	"  0",
	"  0",
	"  0",

	"0000",
	"   0",
	"  0",
	" 0",
	"0",
	"0",
	"0000",

	"",
	"",
	"",
	"",
	"",
	"",
	"0",

	"   0",
	"  0",
	"  0",
	" 0",
	" 0",
	"0",
	"0",
};

internal void draw_text(const char *text, float x, float y, float size, u32 color) {
    float half_size = size * .5f;
    float original_y = y;

    while (*text) {
        if(*text != 32) {
            // text - A because A as a first capital letter is mapped to 65
            const char **a_letter = letters[*text-'A'];
            float original_x = x;

            for (int i = 0; i < 7; i++) {
                const char* row = a_letter[i];
                while(*row){
                    if (*row == '0') {
                        draw_rectangle(x, y, half_size, half_size, color);
                    }
                    x += size;
                    row++;
                }
                y -= size;
                x = original_x;
            }
        }
        text++;
        x += size * 6.f;
        y = original_y;
    }
}

internal void draw_number(int number, float x, float y, float size, u32 color) {
    float half_size = size * .5f;

    bool drew_number = false;

    while (number || !drew_number) {
        int digit = number % 10;
        number /= 10;

        drew_number = true;

        // Number drawing logic
        switch(digit) {
            case 0: {
                draw_rectangle(x - size, y, half_size, 2.5f * size, color);
                draw_rectangle(x + size, y, half_size, 2.5f * size, color);
                draw_rectangle(x, y + size * 2.f, half_size, half_size, color);
                draw_rectangle(x, y - size * 2.f, half_size, half_size, color);
                x -= size * 4.f;
            } break;

            case 1: {
                draw_rectangle(x + size, y, half_size, 2.5f * size, color);
                x -= size * 2.f;
            } break;

            case 2: {
                draw_rectangle(x, y + size * 2.f, 1.5f * size, half_size, color);
                draw_rectangle(x, y, 1.5f * size, half_size, color);
                draw_rectangle(x, y - size * 2.f, 1.5f * size, half_size, color);
                draw_rectangle(x + size, y + size, half_size, half_size, color);
                draw_rectangle(x - size, y - size, half_size, half_size, color);
                x -= size * 4.f;
            } break;

            case 3: {
                draw_rectangle(x - half_size, y + size * 2.f, size, half_size, color);
                draw_rectangle(x - half_size, y, size, half_size, color);
                draw_rectangle(x - half_size, y - size * 2.f, size, half_size, color);
                draw_rectangle(x + size, y, half_size, 2.5f * size, color);
                x -= size * 4.f;
            } break;

            case 4: {
                draw_rectangle(x + size, y, half_size, 2.5f * size, color);
                draw_rectangle(x - size, y + size, half_size, 1.5f * size, color);
                draw_rectangle(x, y, half_size, half_size, color);
                x -= size * 4.f;
            } break;

            case 5: {
                draw_rectangle(x, y + size * 2.f, 1.5f * size, half_size, color);
                draw_rectangle(x, y, 1.5f * size, half_size, color);
                draw_rectangle(x, y - size * 2.f, 1.5f * size, half_size, color);
                draw_rectangle(x - size, y + size, half_size, half_size, color);
                draw_rectangle(x + size, y - size, half_size, half_size, color);
                x -= size * 4.f;
            } break;

            case 6: {
                draw_rectangle(x + half_size, y + size * 2.f, size, half_size, color);
                draw_rectangle(x + half_size, y, size, half_size, color);
                draw_rectangle(x + half_size, y - size * 2.f, size, half_size, color);
                draw_rectangle(x - size, y, half_size, 2.5f * size, color);
                draw_rectangle(x + size, y - size, half_size, half_size, color);
                x -= size * 4.f;
            } break;

            case 7: {
                draw_rectangle(x + size, y, half_size, 2.5f * size, color);
                draw_rectangle(x - half_size, y + size * 2.f, size, half_size, color);
                x -= size * 4.f;
            } break;

            case 8: {
                draw_rectangle(x - size, y, half_size, 2.5f * size, color);
                draw_rectangle(x + size, y, half_size, 2.5f * size, color);
                draw_rectangle(x, y + size * 2.f, half_size, half_size, color);
                draw_rectangle(x, y - size * 2.f, half_size, half_size, color);
                draw_rectangle(x, y, half_size, half_size, color);
                x -= size * 4.f;
            } break;

            case 9: {
                draw_rectangle(x - half_size, y + size * 2.f, size, half_size, color);
                draw_rectangle(x - half_size, y, size, half_size, color);
                draw_rectangle(x - half_size, y - size * 2.f, size, half_size, color);
                draw_rectangle(x + size, y, half_size, 2.5f * size, color);
                draw_rectangle(x - size, y + size, half_size, half_size, color);
                x -= size * 4.f;
            } break;
        }
    }
}